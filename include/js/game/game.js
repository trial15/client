var canvasElement;
var canvas;

var currentTime;
var lastTime;
var deltaTime;

function start() {
	canvasElement = document.getElementById("canvas");
	canvas = canvasElement.getContext("2d");
}

function update() {
	//to be comlpted
}

function draw() {
	canvas.clearRect(0, 0, canvasElement.width, canvasElement.height);
}

function runGame() {
	currentTime = (new Date()).getTime();
	deltaTime = (currentTime - lastTime) / 1000;

	update();
	draw();

	lastTime = currentTime;

	requestAnimationFrame(runGame);
}

window.addEventListener("load", function() {
	start();
	requestAnimationFrame(runGame);
});
